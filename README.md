# Adonis Fun

Tesk task for my next job :)

# Development instructions:
1. Make sure you have [NodeJS v16](https://nodejs.org/dist/v16.13.2/) and Yarn (`npm install --global yarn`) installed
1. Start MySQL+Adminer: `./dc up --build -d`
1. Start local development server: `yarn dev`
1. Copy .env file from example: `cp .env.example .env`
1. Try [this url](http://127.0.0.1:3333)
1. Also try [this url](http://127.0.0.1:8080) (login with .env file credentials)
