FROM node:17-buster

WORKDIR /app

COPY . .

RUN ["yarn"]
RUN ["yarn", "build"]

COPY .env ./build/.env


CMD ["node", "build/server.js"]
