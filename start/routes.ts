/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/
import Route from '@ioc:Adonis/Core/Route';
import { ResourceRouteNames } from '@ioc:Adonis/Core/Route';

// REST Api without "show"
const supportedRESTMethods: ResourceRouteNames[] = ['index', 'store', 'update', 'destroy'];

Route.group(() => {
  Route.resource('/products', 'ProductsController').only(supportedRESTMethods);
  Route.post('/products/:id/images', 'ProductsController.attachImages');
  Route.post('/products/:id/images/set_main', 'ProductsController.setMainImage');
  Route.delete('/products/:id/images', 'ProductsController.deleteImages');
  Route.post('/products/:id/attributes/attach', 'ProductsController.attachAttribute');
  Route.post('/products/:id/attributes/detach', 'ProductsController.detachAttribute');

  Route.resource('/attributes', 'AttributesController').only(supportedRESTMethods);

  Route.resource('/categories', 'CategoriesController').only(supportedRESTMethods);
  Route.post('/categories/:id/add_product', 'CategoriesController.addProduct');
  Route.post('/categories/:id/remove_product', 'CategoriesController.removeProduct');
}).prefix('/admin');

Route.group(() => {
  Route.group(() => {
    Route.group(() => {
      Route.post('/login', 'UsersController.login');
      Route.post('/register', 'UsersController.register');
      Route.get('/me', 'UsersController.getMe');
      Route.get('/activate/:token', 'UsersController.activateUser');
    }).prefix('/users');

    Route.resource('/orders', 'OrdersController').only(['store']);
  }).prefix('/v1');
}).prefix('/api');
