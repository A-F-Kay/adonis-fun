import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';

// TODO: ban any
export interface RESTController {
  index(request: HttpContextContract): any;
  store(request: HttpContextContract): any;
  update(request: HttpContextContract): any;
  destroy(request: HttpContextContract): any;
}
