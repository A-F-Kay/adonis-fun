import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class ProductProductCategories extends BaseSchema {
  protected tableName = 'product_categories';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.timestamps(true, true);

      table.integer('product_id').unsigned().references('id').inTable('products');
      table.integer('category_id').unsigned().references('id').inTable('categories');
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
