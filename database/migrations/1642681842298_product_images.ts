import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class ProductImages extends BaseSchema {
  protected tableName = 'product_images';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.timestamps(true, true);

      table.string('file_name').notNullable().unique();
      table
        .integer('product_id')
        .unsigned()
        .references('id')
        .inTable('products')
        .onDelete('CASCADE');
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
