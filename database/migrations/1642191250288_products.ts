import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Products extends BaseSchema {
  protected tableName = 'products';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.timestamps(true, true);

      table.string('name').notNullable();
      table.integer('price').notNullable();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
