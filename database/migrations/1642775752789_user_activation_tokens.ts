import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class UserActivationTokens extends BaseSchema {
  protected tableName = 'user_activation_tokens';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id');
      table.timestamp('created_at', { useTz: true });

      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE');
      table.string('token', 255).notNullable();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
