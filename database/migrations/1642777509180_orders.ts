import BaseSchema from '@ioc:Adonis/Lucid/Schema';
import { ORDER_PRODUCTS_PIVOT_TABLE_NAME } from 'App/Const/database';

export default class Orders extends BaseSchema {
  protected tableName = 'orders';
  private profilesTableName = 'profiles';

  public async up() {
    this.schema.alterTable(this.profilesTableName, (table) => {
      table.unique(['user_id']);
    });

    this.schema.createTable(this.tableName, (table) => {
      table.increments('id');
      table.timestamps(true, true);

      table.string('first_name').notNullable();
      table.string('last_name').notNullable();
      table.string('phone').notNullable();
      table.string('email').notNullable();

      table
        .integer('profile_id')
        .unsigned()
        .nullable()
        .references('id')
        .inTable('profiles')
        .onDelete('CASCADE');
    });

    this.schema.createTable(ORDER_PRODUCTS_PIVOT_TABLE_NAME, (table) => {
      table.integer('product_id').unsigned().references('id').inTable('products');
      table.integer('order_id').unsigned().references('id').inTable('orders');
      table.integer('price').unsigned().notNullable();
    });
  }

  public async down() {
    this.schema.dropTable(ORDER_PRODUCTS_PIVOT_TABLE_NAME);

    this.schema.dropTable(this.tableName);

    this.schema.alterTable(this.profilesTableName, (table) => {
      table.dropUnique(['user_id']);
    });
  }
}
