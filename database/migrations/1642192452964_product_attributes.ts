import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class ProductAttributes extends BaseSchema {
  protected tableName = 'attributes';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.timestamps(true, true);

      table.string('name').notNullable().unique();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
