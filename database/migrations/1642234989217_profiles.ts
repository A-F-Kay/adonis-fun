import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Profiles extends BaseSchema {
  protected tableName = 'profiles';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id');
      table.timestamps(true, true);

      table.string('first_name', 31).nullable();
      table.string('last_name', 31).nullable();
      table.string('phone', 31).nullable().unique();

      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE');
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
