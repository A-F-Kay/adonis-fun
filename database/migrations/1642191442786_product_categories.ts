import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class ProductCategories extends BaseSchema {
  protected tableName = 'categories';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.timestamps(true, true);

      table.string('name').notNullable().unique();
      table.integer('parent_id').nullable();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
