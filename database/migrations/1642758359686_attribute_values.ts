import BaseSchema from '@ioc:Adonis/Lucid/Schema';
import { PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME } from 'App/Const/database';

export default class AttributeValues extends BaseSchema {
  protected tableName = PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME;

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.string('value').nullable();
    });
  }

  public async down() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('value');
    });
  }
}
