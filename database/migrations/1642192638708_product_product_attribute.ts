import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class ProductProductAttributes extends BaseSchema {
  protected tableName = 'product_attributes';

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary();
      table.timestamps(true, true);

      table.integer('product_id').unsigned().references('id').inTable('products');
      table.integer('attribute_id').unsigned().references('id').inTable('attributes');
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
