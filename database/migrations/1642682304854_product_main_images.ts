import BaseSchema from '@ioc:Adonis/Lucid/Schema';

export default class Product extends BaseSchema {
  protected tableName = 'products';

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('main_image_id').unsigned().unique().nullable();
    });
  }

  public async down() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn('main_image_id');
    });
  }
}
