import BaseSeeder from '@ioc:Adonis/Lucid/Seeder';
import Profile from 'App/Models/Profile';
import User from 'App/Models/User';

type UserPartial = Partial<Omit<User, 'profile'>>;
type UserWithProfileData = UserPartial & { profile: Partial<Profile> };

export default class UserSeeder extends BaseSeeder {
  public static developmentOnly = true;

  public async run() {
    const data: UserWithProfileData[] = [
      {
        // :))
        email: 'dsntmatter0900@gmail.com',
        password: 'admin',
        isAdmin: true,
        isActive: true,
        profile: {
          firstName: 'Artyom',
          lastName: 'Fedchenko',
        },
      },
      {
        // WARNING: Email cannot be checked
        email: 'admin@admin.ru',
        password: 'admin',
        isAdmin: true,
        isActive: true,
        profile: {
          firstName: 'Adminer',
          lastName: 'Adminov',
        },
      },
      {
        email: 'change_this_email@gmail.com',
        password: '123',
        isAdmin: false,
        isActive: true,
        profile: {
          firstName: 'Igor',
        },
      },
    ];

    await Promise.all(
      data.map(async (d) => {
        const { profile: profileData, ...userData } = d;

        const user = await User.create(userData);
        const profile = await Profile.create(profileData);

        profile.related('user').associate(user);
      })
    );
  }
}
