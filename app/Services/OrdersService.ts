import { AuthContract } from '@ioc:Adonis/Addons/Auth';
import Mail from '@ioc:Adonis/Addons/Mail';
import Env from '@ioc:Adonis/Core/Env';

import Order from 'App/Models/Order';
import Product from 'App/Models/Product';
import Profile from 'App/Models/Profile';
import { ValidatedNewOrder } from 'App/Types/order';
import { getBaseURL } from 'App/Utils/path';

export class OrdersService {
  public static async create(auth: AuthContract, payload: ValidatedNewOrder): Promise<Order> {
    await auth.use('api').authenticate();

    const { user } = auth;

    const { productsIdList, ...contactInfo } = payload;

    const products = await Product.query().whereIn('id', productsIdList);
    const orderPrice = products.reduce((acc, product) => acc + product.price, 0);

    const order = await Order.create(contactInfo);
    await Promise.all(
      products.map(async (product) =>
        order.related('products').save(product, true, { price: product.price })
      )
    );

    if (user && user.profileId) {
      const profile = await Profile.findOrFail(user.profileId);

      await order.related('madeBy').associate(profile);
    }

    try {
      await Mail.send((message) => {
        message
          .from(`adonis.fun@${Env.get('MAILGUN_DOMAIN')}`)
          .to(contactInfo.email)
          .subject('Заказ успешно оформлен!').html(`
          <h2>Заказ успешно оформлен на сайте ${getBaseURL().href}!</h2>
          
          <h4>Заказанные товары:</h4>
          <ul>
            ${products.map((product) => {
              return `<li> ${product.name} &ndash; \$${product.price}</li>`;
            })}
          </ul>
          <h5>Общая сумма: ${orderPrice}</h5>

          <h4>Указанные контактные данные:</h4>
          <ul>
            <li>Полное имя: ${contactInfo.firstName} ${contactInfo.lastName}</li>
            <li>Номер телефона: ${contactInfo.phone}</li>
            <li>Email: ${contactInfo.email}</li>
          </ul>

          <h3>Спасибо за покупку :)</h3>
        `);
      });
    } catch (e) {
      console.warn(`Error sending order email with id=${order.id}!`);
    }

    await order.load('products');
    return order;
  }
}
