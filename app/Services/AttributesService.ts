import Attribute from 'App/Models/Attribute';
import { PaginationParams, SearchParams, SortParams } from 'App/Types/request_params';
import { PaginatedResponse } from 'App/Types/response';
import { getPaginatedResult } from './common';

export default class AttributesService {
  public static async list({
    search,
    sortBy,
    ...paginationParams
  }: SearchParams & PaginationParams & SortParams): Promise<PaginatedResponse<Attribute>> {
    const query = Attribute.query();

    if (sortBy) {
      const direction = sortBy.startsWith('-') ? 'desc' : 'asc';
      const columnName = sortBy.startsWith('-') ? sortBy.substring(1) : sortBy;

      // TODO: validate is columnName exists

      query.orderBy(columnName, direction);
    }

    if (search) {
      query.where('name', 'like', `${search}%`);
    }

    return await getPaginatedResult(query, paginationParams);
  }

  public static async create(newAttribute: Attribute): Promise<Attribute> {
    return await newAttribute.save();
  }

  public static async update(update: Attribute): Promise<Attribute> {
    const obj = await Attribute.findOrFail(update.id);
    obj.name = update.name;

    return await obj.save();
  }

  public static async deleteById(id: number) {
    const obj = await Attribute.findOrFail(id);
    await obj.delete();
  }
}
