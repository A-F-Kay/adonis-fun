import Mail from '@ioc:Adonis/Addons/Mail';
import Env from '@ioc:Adonis/Core/Env';
import Hash from '@ioc:Adonis/Core/Hash';
import { AuthContract, OpaqueTokenContract } from '@ioc:Adonis/Addons/Auth';
import crypto from 'crypto';
import { URL } from 'url';

import Profile from 'App/Models/Profile';
import User from 'App/Models/User';
import { AuthCredentials, ValidatedNewUser } from 'App/Types/auth';
import UserActivationToken from 'App/Models/UserActivationToken';
import { getBaseURL } from 'App/Utils/path';

export class UsersService {
  public static async createNewUser({
    email,
    password,
    ...profilePayload
  }: ValidatedNewUser): Promise<void> {
    if ((await User.findBy('email', email)) !== null) {
      throw new Error('User with such email already exists!');
    }

    const user = await User.create({
      email,
      password, // Hashes in User.beforeSave
    });

    const profile = await Profile.create({ ...profilePayload });
    await profile.related('user').associate(user);

    const activationToken = await UserActivationToken.create({
      token: crypto.randomBytes(125).toString('hex'),
    });
    await activationToken.related('user').associate(user);

    const link = new URL(`/api/v1/users/activate/${activationToken.token}`, getBaseURL());
    console.log(activationToken.token);
    console.log(link);

    try {
      await Mail.send((message) => {
        message
          .from(`adonis.fun@${Env.get('MAILGUN_DOMAIN')}`)
          .to(user.email)
          .subject('Активация пользователя')
          .text(`Для активации аккаунта перейдите по ссылке: ${link.href}`);
      });
    } catch (e) {
      console.warn(`Error sending activation token message for user with id=${user.id}`);
    }
  }

  public static async activateUser(tokenString: string): Promise<User> {
    const token = await UserActivationToken.findBy('token', tokenString);
    if (!token) {
      throw new Error('Wrong token!');
    }

    const user = await User.find(token.userId);
    token.delete();

    if (!user) {
      throw new Error('Пользователь удален!');
    }

    user.isActive = true;
    await user.save();

    return UsersService.loadUserProfile(user);
  }

  public static async getTokenByCredentials(
    auth: AuthContract,
    { email, password: rawPassword }: AuthCredentials
  ): Promise<OpaqueTokenContract<User>> {
    const user = await User.query().where('email', email).where('is_active', true).first();

    const isValid = user !== null && Boolean(await Hash.verify(user.password, rawPassword));

    if (!isValid) {
      throw new Error('Invalid login credentials!');
    }

    return await auth.use('api').generate(user);
  }

  public static async getMe(auth: AuthContract) {
    // TODO: move authenticate to middleware?
    await auth.use('api').authenticate();

    const { user } = auth;

    if (!user) {
      throw new Error("You're not logged in!");
    }

    return await UsersService.loadUserProfile(user);
  }

  private static async loadUserProfile(user: User): Promise<User> {
    await user.load('profile');

    return user;
  }
}
