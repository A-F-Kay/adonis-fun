import { ModelQueryBuilderContract } from '@ioc:Adonis/Lucid/Orm';
import { PaginationParams } from 'App/Types/request_params';
import { LucidModel } from '@ioc:Adonis/Lucid/Orm';
import { PaginatedResponse } from 'App/Types/response';

export async function getPaginatedResult<ModelType extends LucidModel, ResultType>(
  query: ModelQueryBuilderContract<ModelType, ResultType>,
  { page, limit, unlimit }: PaginationParams
): Promise<PaginatedResponse<ResultType>> {
  if (unlimit) {
    const data = await query;

    return {
      data,
      page: 1,
      limit: data.length,
      total: data.length,
    };
  }

  const paginatedResult = await query.paginate(page, limit);

  return {
    data: paginatedResult.all(),
    page: paginatedResult.currentPage,
    limit: paginatedResult.perPage,
    total: paginatedResult.total,
  };
}
