import Application from '@ioc:Adonis/Core/Application';
import Product from 'App/Models/Product';
import ProductImage from 'App/Models/ProductImage';
import { PaginationParams } from 'App/Types/request_params';
import { PaginatedResponse } from 'App/Types/response';
import { getPaginatedResult } from './common';
import { MultipartFileContract } from '@ioc:Adonis/Core/BodyParser';
import { generateProductImageFileName } from 'App/Utils/path';
import { ValidatedAttributeIdPayload, ValidatedProductAttributePayload } from 'App/Types/product';
import Attribute from 'App/Models/Attribute';
import { PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN } from 'App/Const/database';

export default class ProductsService {
  public static async list(
    paginationParams: PaginationParams
  ): Promise<PaginatedResponse<Product>> {
    const query = Product.query();

    const paginatedResult = await getPaginatedResult(query, paginationParams);
    paginatedResult.data = await Promise.all(
      paginatedResult.data.map(async (product) => ProductsService.loadProductRelations(product))
    );

    return paginatedResult;
  }

  public static async create(newProduct: Product): Promise<Product> {
    return await newProduct.save();
  }

  public static async update(update: Product): Promise<Product> {
    const obj = await Product.findOrFail(update.id);
    obj.name = update.name;
    obj.price = update.price;
    await obj.save();

    return await ProductsService.getFullProductById(update.id);
  }

  public static async attachImages(
    productId: number,
    images: MultipartFileContract[]
  ): Promise<Product> {
    const product = await Product.findOrFail(productId);

    images.forEach((img) => {
      if (!img.extname) {
        return; // TODO: collect warnings and return in API
      }

      const fileName = generateProductImageFileName(img);

      img.moveToDisk(Application.publicPath('images'), { name: fileName });

      const productImage = new ProductImage();
      productImage.fileName = fileName;
      product.related('images').save(productImage);
    });

    return await ProductsService.getFullProductById(productId);
  }

  public static async setMainImage(productId: number, mainImageId: number): Promise<Product> {
    const product = await Product.findOrFail(productId);
    const image = await ProductImage.findOrFail(mainImageId);

    if (image.productId !== product.id) {
      throw new Error('Cannot set main image which does not belong to product!');
    }
    product.mainImageId = mainImageId;
    await product.save();

    return await ProductsService.getFullProductById(productId);
  }

  public static async deleteImages(productId: number, imagesIdList: number[]): Promise<Product> {
    await Product.findOrFail(productId);

    await ProductImage.query().whereIn('id', imagesIdList).where('product_id', productId).delete();

    // TODO: Check if deleted exact amount of rows

    return await ProductsService.getFullProductById(productId);
  }

  public static async attachAttribute(
    productId: number,
    { attributeId, value }: ValidatedProductAttributePayload
  ): Promise<Product> {
    const product = await Product.findOrFail(productId);
    const attribute = await Attribute.findOrFail(attributeId);

    await product.related('attributes').save(attribute, undefined, { value });

    return await ProductsService.loadProductRelations(product);
  }

  public static async detachAttribute(
    productId: number,
    { attributeId }: ValidatedAttributeIdPayload
  ): Promise<Product> {
    const product = await Product.findOrFail(productId);
    const attribute = await Attribute.findOrFail(attributeId);

    await product.related('attributes').detach([attribute.id]);

    return await ProductsService.loadProductRelations(product);
  }

  public static async deleteById(id: number) {
    const obj = await Product.findOrFail(id);
    await obj.delete();
  }

  private static async loadProductRelations(product: Product) {
    await product.load('images');
    await product.load('categories');
    await product.load('attributes', (query) =>
      query.pivotColumns([PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN])
    );

    return product;
  }

  private static async getFullProductById(productId: number) {
    const product = await Product.findOrFail(productId);

    return await ProductsService.loadProductRelations(product);
  }
}
