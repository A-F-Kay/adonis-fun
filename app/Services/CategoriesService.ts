import Category from 'App/Models/Category';
import Product from 'App/Models/Product';
import { PaginationParams } from 'App/Types/request_params';
import { PaginatedResponse } from 'App/Types/response';
import { getPaginatedResult } from './common';

export type IdWithProductIdPayload = {
  id: number;
  productId: number;
};

export default class CategoriesService {
  public static async list(
    paginationParams: PaginationParams
  ): Promise<PaginatedResponse<Category>> {
    const query = Category.query();

    const paginatedResult = await getPaginatedResult(query, paginationParams);

    await Promise.all(paginatedResult.data.map(CategoriesService.loadRelations));
    return paginatedResult;
  }

  public static async create(newCategory: Category): Promise<Category> {
    await CategoriesService.checkForParent(newCategory);

    await newCategory.save();

    return CategoriesService.loadRelations(newCategory);
  }

  public static async update(update: Category): Promise<Category> {
    const category = await Category.findOrFail(update.id);

    await CategoriesService.checkForParent(update);

    category.name = update.name;
    category.parentId = update.parentId;
    await category.save();

    return CategoriesService.loadRelations(category);
  }

  public static async addProduct({ id, productId }: IdWithProductIdPayload): Promise<Category> {
    const product = await Product.findOrFail(productId);
    const category = await Category.findOrFail(id);

    await category.related('products').save(product);
    await category.save();

    return CategoriesService.loadRelations(category);
  }

  public static async removeProduct({ id, productId }: IdWithProductIdPayload): Promise<Category> {
    const product = await Product.findOrFail(productId);
    const category = await Category.findOrFail(id);

    await category.related('products').detach([product.id]);
    await category.save();

    return CategoriesService.loadRelations(category);
  }

  public static async deleteById(id: number) {
    const obj = await Category.findOrFail(id);

    // Delete subcategories
    await Category.query().where('parent_id', obj.id).delete();
    await obj.delete();
  }

  private static async loadRelations(category: Category): Promise<Category> {
    await category.load('products');

    return category;
  }

  private static async checkForParent(category: Category): Promise<void> {
    const { parentId } = category;

    if (parentId === category.id) {
      throw new Error('parentId cannot reference the object itself!');
    }

    if (parentId) {
      const parent = await Category.findOrFail(parentId);
      if (parent.parentId === category.id) {
        throw new Error('Cannot make recursive parentId!');
      }
    }
  }
}
