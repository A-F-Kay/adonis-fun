import { URL } from 'url';
import { v4 as uuid } from 'uuid';

import Application from '@ioc:Adonis/Core/Application';
import Env from '@ioc:Adonis/Core/Env';
import { MultipartFileContract } from '@ioc:Adonis/Core/BodyParser';

const defaultHost = 'localhost';
const defaultPort = 80;

export function getBaseURL(): URL {
  let host = Env.get('HOST', defaultHost);
  if (host === '0.0.0.0') {
    host = defaultHost;
  }

  const port = Env.get('PORT', defaultPort);

  return new URL(`http://${host}:${port}/`);
}

export function getProductImagesURL(): URL {
  return new URL(Application.publicPath('images'), getBaseURL());
}

export function generateProductImageFileName(img: MultipartFileContract): string {
  return `${Number(new Date())}_${uuid()}.${img.extname}`;
}
