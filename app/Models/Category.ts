import { DateTime } from 'luxon';
import { BaseModel, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm';
import Product from './Product';
import { PRODUCT_CATEGORIES_PIVOT_TABLE_NAME } from 'App/Const/database';

export default class Category extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column()
  public name: string;

  @column()
  public parentId: null | number;

  @manyToMany(() => Product, { pivotTable: PRODUCT_CATEGORIES_PIVOT_TABLE_NAME })
  public products: ManyToMany<typeof Product>;
}
