import { DateTime } from 'luxon';
import { BaseModel, column, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm';
import Product from './Product';
import {
  PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME,
  PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN,
} from 'App/Const/database';

export default class Attribute extends BaseModel {
  public serializeExtras() {
    return {
      value: this.$extras.pivot_value,
    };
  }

  @column({ isPrimary: true })
  public id: number;

  @column.dateTime({ autoCreate: true, serializeAs: null })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: null })
  public updatedAt: DateTime;

  @column()
  public name: string;

  @manyToMany(() => Product, {
    pivotTable: PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME,
    pivotColumns: [PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN],
    pivotTimestamps: true,
  })
  public products: ManyToMany<typeof Product>;
}
