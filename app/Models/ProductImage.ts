import path from 'path';
import Drive from '@ioc:Adonis/Core/Drive';
import Product from './Product';
import { DateTime } from 'luxon';
import { afterDelete, BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm';
import { URL } from 'url';
import { getBaseURL } from 'App/Utils/path';

export default class ProductImage extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column({ serialize: (fname: string) => new URL(`/public/images/${fname}`, getBaseURL()) })
  public fileName: string;

  // TODO: Is it necessary since we have 'product' field?
  @column()
  public productId: number;

  @belongsTo(() => Product)
  public product: BelongsTo<typeof Product>;

  // FIXME: Doesn't work
  @afterDelete()
  public static async deleteAssociatedFile(image: ProductImage) {
    const filePath = path.join('public/images', image.fileName);

    await Drive.delete(filePath);
  }
}
