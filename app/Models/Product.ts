import { DateTime } from 'luxon';
import { BaseModel, column, HasMany, hasMany, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm';
import Category from './Category';
import Attribute from './Attribute';
import {
  ORDER_PRODUCTS_PIVOT_TABLE_NAME,
  PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME,
  PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN,
  PRODUCT_CATEGORIES_PIVOT_TABLE_NAME,
} from 'App/Const/database';
import ProductImage from './ProductImage';
import Order from './Order';

export default class Product extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column()
  public name: string;

  @column()
  public price: number;

  @manyToMany(() => Attribute, {
    pivotTable: PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME,
    pivotColumns: [PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN],
    pivotTimestamps: true,
  })
  public attributes: ManyToMany<typeof Attribute>;

  @manyToMany(() => Category, { pivotTable: PRODUCT_CATEGORIES_PIVOT_TABLE_NAME })
  public categories: ManyToMany<typeof Category>;

  @hasMany(() => ProductImage)
  public images: HasMany<typeof ProductImage>;

  @manyToMany(() => Order, {
    pivotTable: ORDER_PRODUCTS_PIVOT_TABLE_NAME,
    pivotColumns: ['price'],
    serializeAs: null,
  })
  public orders: ManyToMany<typeof Order>;

  @column()
  public mainImageId: null | number;
}
