import { DateTime } from 'luxon';
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm';
import Profile from './Profile';
import Product from './Product';
import { ORDER_PRODUCTS_PIVOT_TABLE_NAME } from 'App/Const/database';

export default class Order extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column()
  public firstName: string;

  @column()
  public lastName: string;

  @column()
  public phone: string;

  @column()
  public email: string;

  @manyToMany(() => Product, {
    pivotTable: ORDER_PRODUCTS_PIVOT_TABLE_NAME,
    pivotColumns: ['price'],
  })
  public products: ManyToMany<typeof Product>;

  @column()
  public profileId: null | number;

  @belongsTo(() => Profile, {
    foreignKey: 'profileId',
  })
  public madeBy: BelongsTo<typeof Profile>;
}
