import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { OrdersService } from 'App/Services/OrdersService';
import { validateNewOrder } from 'App/Validators/Orders';

export default class OrdersController {
  public async store({ auth, request }: HttpContextContract) {
    const payload = await validateNewOrder(request);

    const newOrder = await OrdersService.create(auth, payload);

    return newOrder.serialize();
  }
}
