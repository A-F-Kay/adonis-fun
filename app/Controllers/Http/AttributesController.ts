import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import AttributesService from 'App/Services/AttributesService';
import {
  validateIdParam,
  validatePaginationParams,
  validateSearchParams,
  validateSortParams,
} from 'App/Validators/common';
import { validateAttributeEdit, validateNewAttribute } from 'App/Validators/Attributes';
import { RESTController } from 'Contracts/rest_controller';

// TODO: Please search for swagger auto-doc. Manual doc writing is painful :(
export default class AttributesController implements RESTController {
  /**
   * @swagger
   * /api/v1/attributes:
   *   get:
   *     tags:
   *       - Attributes
   *     summary: Get list of product attributes
   *     responses:
   *       200:
   *         description: Product attributes array
   *         example:
   *           message: [AttributeModel]
   */
  public async index({ request }: HttpContextContract) {
    const params = {
      ...(await validateSearchParams(request)),
      ...(await validatePaginationParams(request)),
      ...(await validateSortParams(request)),
    };

    const paginatedResponse = await AttributesService.list(params);

    return {
      ...paginatedResponse,
      data: paginatedResponse.data.map((d) => d.serialize()),
    };
  }

  /**
   * @swagger
   * /api/v1/attributes:
   *   post:
   *     tags:
   *       - Attributes
   *     summary: Create new attribute
   *     parameters:
   *       - name: name
   *         in:  body
   *         required: true
   *         schema:
   *           properties:
   *             name:
   *               type: string
   *               example: 'Размер'
   *               required: true
   *             products:
   *               type: [integer]
   *               example: []
   *               required: false
   *     responses:
   *       200:
   *         description: Attribute successfully added
   *         example:
   *           message: AttributeModel
   */
  public async store({ request }: HttpContextContract) {
    const payload = await validateNewAttribute(request);

    const newAttribute = await AttributesService.create(payload);

    return newAttribute.serialize();
  }

  /**
   * @swagger
   * /api/v1/attributes/:id:
   *   put:
   *     tags:
   *       - Attributes
   *     summary: Update existing attribute
   *     responses:
   *       200:
   *         description: Attribute successfully updated
   *         example:
   *           message: AttributeModel
   */
  public async update({ request }: HttpContextContract) {
    const payload = await validateAttributeEdit(request);

    const attribute = await AttributesService.update(payload);

    return attribute.serialize();
  }

  /**
   * @swagger
   * /api/v1/attributes/:id:
   *   delete:
   *     tags:
   *       - Attributes
   *     summary: Remove existing attribute
   *     responses:
   *       200:
   *         description: Attribute successfully removed
   *         example:
   *           message: {}
   */
  public async destroy({ request }: HttpContextContract) {
    const id = validateIdParam(request);

    await AttributesService.deleteById(id);

    return { success: true };
  }
}
