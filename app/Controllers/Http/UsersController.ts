import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import { validateCredentials, validateNewUser } from 'App/Validators/Auth';
import { UsersService } from 'App/Services/UsersService';

export default class UsersController {
  public async login({ auth, request }: HttpContextContract) {
    const payload = await validateCredentials(request);

    const token = await UsersService.getTokenByCredentials(auth, payload);

    return token.toJSON();
  }

  public async register({ auth, request, response }: HttpContextContract) {
    if (auth.isLoggedIn) {
      return response.badRequest({ error: 'Please log out first!' });
    }

    const payload = await validateNewUser(request);

    await UsersService.createNewUser(payload);

    return { success: true, message: 'Check email for token' };
  }

  public async activateUser({ auth, request, response }: HttpContextContract) {
    if (auth.isLoggedIn) {
      return response.badRequest({ error: 'Please log out first!' });
    }

    const tokenString = String(request.param('token', ''));

    const user = await UsersService.activateUser(tokenString);

    // TODO: There must be cool HTML string probably :)
    return user.serialize();
  }

  public async getMe({ auth, response }: HttpContextContract) {
    try {
      return await UsersService.getMe(auth);
    } catch ({ message }) {
      return response.badRequest({ error: message });
    }
  }
}
