import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import ProductsService from 'App/Services/ProductsService';
import { validateIdParam, validatePaginationParams } from 'App/Validators/common';
import {
  validateNewProduct,
  validateProductEdit,
  validateProductImages,
  validateProductMainImageId,
  validateProductImagesIdList,
  validateProductAttribute,
  validateProductAttributeId,
} from 'App/Validators/Products';
import { RESTController } from 'Contracts/rest_controller';

export default class ProductsController implements RESTController {
  public async index({ request }: HttpContextContract) {
    const params = await validatePaginationParams(request);

    const paginatedResponse = await ProductsService.list(params);

    return {
      ...paginatedResponse,
      data: paginatedResponse.data.map((d) => d.serialize()),
    };
  }

  public async store({ request }: HttpContextContract) {
    const payload = await validateNewProduct(request);

    const newProduct = await ProductsService.create(payload);

    return newProduct.serialize();
  }

  public async attachImages({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const images = await validateProductImages(request);

    const product = await ProductsService.attachImages(id, images);

    return product.serialize();
  }

  public async setMainImage({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const mainImageId = await validateProductMainImageId(request);

    const product = await ProductsService.setMainImage(id, mainImageId);

    return product.serialize();
  }

  public async deleteImages({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const imagesIdList = await validateProductImagesIdList(request);

    const product = await ProductsService.deleteImages(id, imagesIdList);

    return product.serialize();
  }

  public async attachAttribute({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const productAttribute = await validateProductAttribute(request);

    const product = await ProductsService.attachAttribute(id, productAttribute);

    return product.serialize();
  }

  public async detachAttribute({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const productAttributeId = await validateProductAttributeId(request);

    const product = await ProductsService.detachAttribute(id, productAttributeId);

    return product.serialize();
  }

  public async update({ request }: HttpContextContract) {
    const payload = await validateProductEdit(request);

    const product = await ProductsService.update(payload);

    return product.serialize();
  }

  public async destroy({ request }: HttpContextContract) {
    const id = validateIdParam(request);

    await ProductsService.deleteById(id);

    return { success: true };
  }
}
