import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext';
import CategoriesService from 'App/Services/CategoriesService';
import {
  validateCategoryEdit,
  validateNewCategory,
  validateProductId,
} from 'App/Validators/Categories';
import { validateIdParam, validatePaginationParams } from 'App/Validators/common';
import { RESTController } from 'Contracts/rest_controller';

export default class CategoriesController implements RESTController {
  public async index({ request }: HttpContextContract) {
    const params = await validatePaginationParams(request);

    const paginatedResponse = await CategoriesService.list(params);

    return {
      ...paginatedResponse,
      data: paginatedResponse.data.map((d) => d.serialize()),
    };
  }

  public async store({ request }: HttpContextContract) {
    const payload = await validateNewCategory(request);

    const newProduct = await CategoriesService.create(payload);

    return newProduct.serialize();
  }

  public async update({ request }: HttpContextContract) {
    const payload = await validateCategoryEdit(request);

    const product = await CategoriesService.update(payload);

    return product.serialize();
  }

  public async addProduct({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const productId = await validateProductId(request);

    const category = await CategoriesService.addProduct({ id, productId });

    return category.serialize();
  }

  public async removeProduct({ request }: HttpContextContract) {
    const id = validateIdParam(request);
    const productId = await validateProductId(request);

    const category = await CategoriesService.removeProduct({ id, productId });

    return category.serialize();
  }

  public async destroy({ request }: HttpContextContract) {
    const id = validateIdParam(request);

    await CategoriesService.deleteById(id);

    return { success: true };
  }
}
