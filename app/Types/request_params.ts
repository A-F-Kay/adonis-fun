export type PaginationParams = {
  page: number;
  limit: number;
  unlimit?: boolean;
};

export type SearchParams = {
  search?: string;
};

export type SortParams = {
  sortBy?: string;
};
