export type ValidatedNewOrder = {
  productsIdList: number[];
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
};
