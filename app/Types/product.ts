export type ValidatedAttributeIdPayload = {
  attributeId: number;
};

export type ValidatedProductAttributePayload = ValidatedAttributeIdPayload & {
  value?: string;
};
