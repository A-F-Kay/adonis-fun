export type AuthCredentials = {
  email: string;
  password: string;
};

export type ValidatedNewUser = AuthCredentials & {
  firstName?: string;
  lastName?: string;
  phone?: string;
};
