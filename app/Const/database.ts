export const DEFAULT_LIMIT = 100;

export const PRODUCT_ATTRIBUTES_PIVOT_TABLE_NAME = 'product_attributes';
export const PRODUCT_CATEGORIES_PIVOT_TABLE_NAME = 'product_categories';
export const ORDER_PRODUCTS_PIVOT_TABLE_NAME = 'order_products';

export const PRODUCT_ATTRIBUTE_VALUE_PIVOT_COLUMN = 'value';
