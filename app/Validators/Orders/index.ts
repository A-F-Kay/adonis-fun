import { RequestContract } from '@ioc:Adonis/Core/Request';
import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { ValidatedNewOrder } from 'App/Types/order';
import { emailSchema } from '../common';

const newOrderSchema = {
  ...emailSchema,
  firstName: schema.string({ trim: true }, [rules.minLength(2)]),
  lastName: schema.string({ trim: true }, [rules.minLength(2)]),
  phone: schema.string({ trim: true }, [rules.mobile()]),
  productsIdList: schema.array([rules.minLength(1)]).members(schema.number([rules.unsigned()])),
};

export async function validateNewOrder(request: RequestContract): Promise<ValidatedNewOrder> {
  const orderSchema = schema.create(newOrderSchema);

  return await request.validate({ schema: orderSchema });
}
