import { RequestContract } from '@ioc:Adonis/Core/Request';
import { schema, rules } from '@ioc:Adonis/Core/Validator';
import Product from 'App/Models/Product';
import { idSchema, nameSchema } from '../common';
import { MultipartFileContract } from '@ioc:Adonis/Core/BodyParser';
import { ValidatedAttributeIdPayload, ValidatedProductAttributePayload } from 'App/Types/product';

const newProductSchema = {
  ...nameSchema,
  price: schema.number([rules.unsigned()]),
};

const attributeIdSchema = {
  attributeId: schema.number([rules.unsigned()]),
};

export async function validateNewProduct(request: RequestContract): Promise<Product> {
  const productSchema = schema.create(newProductSchema);
  const { name, price } = await request.validate({ schema: productSchema });

  const product = new Product();
  product.name = name;
  product.price = price;

  return product;
}

export async function validateProductEdit(request: RequestContract): Promise<Product> {
  const productSchema = schema.create({
    ...idSchema,
    ...newProductSchema,
  });
  const payload = await request.validate({ schema: productSchema });

  if (payload.id !== Number(request.param('id'))) {
    throw new Error('Please provide valid id!');
  }

  const product = new Product();
  product.id = payload.id;
  product.name = payload.name;
  product.price = payload.price;

  return product;
}

export async function validateProductImages(
  request: RequestContract
): Promise<MultipartFileContract[]> {
  const imagesSchema = schema.create({
    images: schema.array([rules.minLength(1)]).members(
      schema.file({
        size: '2mb',
        extnames: ['jpg', 'png', 'jpeg'],
      })
    ),
  });

  const { images } = await request.validate({ schema: imagesSchema });

  return images;
}

export async function validateProductMainImageId(request: RequestContract): Promise<number> {
  const mainImageSchema = schema.create({
    mainImageId: schema.number([rules.unsigned()]),
  });

  const { mainImageId } = await request.validate({ schema: mainImageSchema });

  return mainImageId;
}

export async function validateProductImagesIdList(request: RequestContract): Promise<number[]> {
  const imagesDeleteSchema = schema.create({
    idList: schema.array([rules.minLength(1)]).members(schema.number([rules.unsigned()])),
  });

  const { idList } = await request.validate({ schema: imagesDeleteSchema });

  return idList;
}

export async function validateProductAttribute(
  request: RequestContract
): Promise<ValidatedProductAttributePayload> {
  const attributeSchema = schema.create({
    ...attributeIdSchema,
    value: schema.string.optional(),
  });

  return await request.validate({ schema: attributeSchema });
}

export async function validateProductAttributeId(
  request: RequestContract
): Promise<ValidatedAttributeIdPayload> {
  const attributeSchema = schema.create(attributeIdSchema);

  return await request.validate({ schema: attributeSchema });
}
