import { RequestContract } from '@ioc:Adonis/Core/Request';
import { schema, rules } from '@ioc:Adonis/Core/Validator';
import Category from 'App/Models/Category';
import { idSchema, nameSchema } from '../common';

const newCategorySchema = {
  ...nameSchema,
  parentId: schema.number.optional([rules.unsigned()]),
};

export async function validateNewCategory(request: RequestContract): Promise<Category> {
  const categorySchema = schema.create(newCategorySchema);
  const payload = await request.validate({ schema: categorySchema });

  const category = new Category();
  category.name = payload.name;
  category.parentId = payload.parentId ?? null;

  return category;
}

export async function validateCategoryEdit(request: RequestContract): Promise<Category> {
  const categorySchema = schema.create({
    ...idSchema,
    ...newCategorySchema,
  });
  const payload = await request.validate({ schema: categorySchema });

  if (payload.id !== Number(request.param('id'))) {
    throw new Error('Please provide valid id!');
  }

  const category = new Category();
  category.id = payload.id;
  category.name = payload.name;
  category.parentId = payload.parentId ?? null;

  return category;
}

export async function validateProductId(request: RequestContract): Promise<number> {
  const productIdSchema = schema.create({
    productId: schema.number([rules.unsigned()]),
  });
  const { productId } = await request.validate({ schema: productIdSchema });

  return productId;
}
