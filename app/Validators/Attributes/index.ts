import { RequestContract } from '@ioc:Adonis/Core/Request';
import { schema } from '@ioc:Adonis/Core/Validator';
import Attribute from 'App/Models/Attribute';
import { idSchema, nameSchema } from '../common';

export async function validateNewAttribute(request: RequestContract): Promise<Attribute> {
  const attributeSchema = schema.create({
    ...nameSchema,
  });
  const payload = await request.validate({ schema: attributeSchema });

  const attribute = new Attribute();
  attribute.name = payload.name;

  return attribute;
}

export async function validateAttributeEdit(request: RequestContract): Promise<Attribute> {
  const attributeSchema = schema.create({
    ...idSchema,
    ...nameSchema,
  });
  const payload = await request.validate({ schema: attributeSchema });

  if (payload.id !== Number(request.param('id'))) {
    throw new Error('Please provide valid id!');
  }

  const attribute = new Attribute();
  attribute.id = payload.id;
  attribute.name = payload.name;

  return attribute;
}
