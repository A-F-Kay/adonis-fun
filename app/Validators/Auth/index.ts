import { RequestContract } from '@ioc:Adonis/Core/Request';
import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { AuthCredentials, ValidatedNewUser } from 'App/Types/auth';
import { emailSchema, profileSchema } from '../common';

const credentialsSchema = {
  ...emailSchema,
  password: schema.string({}, [rules.minLength(5)]),
};

export async function validateNewUser(request: RequestContract): Promise<ValidatedNewUser> {
  const newUserSchema = schema.create({
    ...credentialsSchema,
    ...profileSchema,
  });

  return await request.validate({ schema: newUserSchema });
}

export async function validateCredentials(request: RequestContract): Promise<AuthCredentials> {
  const credSchema = schema.create(credentialsSchema);

  return await request.validate({ schema: credSchema });
}
