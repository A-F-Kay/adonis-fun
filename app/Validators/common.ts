import { RequestContract } from '@ioc:Adonis/Core/Request';
import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { DEFAULT_LIMIT } from 'App/Const/database';
import { PaginationParams, SearchParams, SortParams } from 'App/Types/request_params';

export const idSchema = {
  id: schema.number([rules.unsigned()]),
};

export const nameSchema = {
  name: schema.string({ trim: true }),
};

export const profileSchema = {
  firstName: schema.string.optional({ trim: true }, [rules.minLength(2)]),
  lastName: schema.string.optional({ trim: true }, [rules.minLength(2)]),
  phone: schema.string.optional({ trim: true }, [rules.mobile()]),
};

export const emailSchema = {
  email: schema.string({ trim: true }, [rules.email()]),
};

export function validateCustomIdParam(request: RequestContract, paramName: string) {
  const idParam = Number(request.param(paramName));

  if (isNaN(idParam) || idParam < 1) {
    throw new Error('Please provide valid id!');
  }

  return idParam;
}

export function validateIdParam(request: RequestContract): number {
  return validateCustomIdParam(request, 'id');
}

export async function validateSearchParams(request: RequestContract): Promise<SearchParams> {
  const searchSchema = schema.create({
    search: schema.string.optional(),
  });

  return await request.validate({ schema: searchSchema });
}

export async function validatePaginationParams(
  request: RequestContract
): Promise<PaginationParams> {
  const paginationSchema = schema.create({
    page: schema.number.optional([rules.unsigned()]),
    limit: schema.number.optional([rules.unsigned()]),
    unlimit: schema.boolean.optional(),
  });

  const payload = await request.validate({ schema: paginationSchema });

  return {
    ...payload,
    page: payload.page ?? 1,
    limit: payload.limit ?? DEFAULT_LIMIT,
  };
}

export async function validateSortParams(request: RequestContract): Promise<SortParams> {
  const sortSchema = schema.create({
    sortBy: schema.string.optional(),
  });

  return await request.validate({ schema: sortSchema });
}
